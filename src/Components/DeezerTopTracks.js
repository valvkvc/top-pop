import React from 'react';
import ListOrderSelector from './ListOrderSelector';
import Track from './Track';
import TrackDetailsModal from './TrackDetailsModal';

const deezerUrl =
    'https://cors-anywhere.herokuapp.com/https://api.deezer.com/chart/0/tracks';

export default class DeezerTopTenTracks extends React.Component {
    constructor() {
        super();

        this.state = {
            error: null,
            isLoaded: false,
            sortOrder: 'ascending',
            data: [],
            isModalOpen: false,
            trackDetails: {},
        };
    }

    sortBy(sortOrder) {
        const data = [...this.state.data];
        if (sortOrder === 'ascending') {
            data.sort((trackA, trackB) => trackA.duration - trackB.duration);
            this.setState({ data });
        } else if (sortOrder === 'descending') {
            data.sort((trackA, trackB) => trackB.duration - trackA.duration);
            this.setState({ data });
        }
    }

    handleSortOrderChange = (sortOrder) => {
        this.setState({ sortOrder });
        this.sortBy(sortOrder);
    };

    handleItemClick = (id) => {
        this.setState({ isModalOpen: true });

        const selectedTrack = this.state.data.find(
            (element) => element.id === id
        );
        this.setState({ trackDetails: selectedTrack });
    };

    handleModalClose = () => {
        this.setState({ isModalOpen: false });
    };

    componentDidMount() {
        fetch(deezerUrl)
            .then((response) => response.json())
            .then(
                (results) => {
                    this.setState({
                        isLoaded: true,
                        data: results.data,
                    });
                    this.sortBy(this.state.sortOrder);
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error,
                    });
                }
            );
    }

    render() {
        const trackDetailsModal = this.state.isModalOpen ? (
            <TrackDetailsModal
                trackDetails={this.state.trackDetails}
                onClose={this.handleModalClose}
            />
        ) : null;

        if (this.state.error) {
            return <div>Error: {this.state.error.message}</div>;
        } else if (!this.state.isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div
                    className={`main-content-${
                        this.state.isModalOpen ? 'disabled' : ''
                    }`}
                >
                    <ListOrderSelector
                        sortOrder={this.state.sortOrder}
                        onSortOrderChange={this.handleSortOrderChange}
                    />
                    <ul>
                        {this.state.data.map((item) => (
                            <Track
                                key={item.id}
                                id={item.id}
                                trackTitle={item.title}
                                onTrackClick={this.handleItemClick}
                            />
                        ))}
                    </ul>
                    {trackDetailsModal}
                </div>
            );
        }
    }
}
