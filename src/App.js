import React from 'react';
import './App.css';
import DeezerTopTracks from './Components/DeezerTopTracks';

function App() {
    return (
        <div className="App">
            <div className="main">
                <div className="app-description">
                    <h3>
                        top ten tracks on <span className="deezer">deezer</span>{' '}
                        atm
                    </h3>
                </div>
                <h1>Top Pop</h1>
                <DeezerTopTracks />
            </div>
            <footer>
                <div className="footer">
                    <img
                        src={'/images/deezer_logo_white.png'}
                        alt="deezer logo"
                        className="deezer-logo"
                    />
                </div>
            </footer>
        </div>
    );
}

export default App;
