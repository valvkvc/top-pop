import React from 'react';

export default class TrackDetailsModal extends React.Component {
    durationFormatter = (duration) => {
        let hours = Math.floor(duration / 3600);
        let minutes = Math.floor(duration / 60) - hours * 60;
        let seconds = duration % 60;

        hours = hours.toString().padStart(2, 0);
        minutes = minutes.toString().padStart(2, 0);
        seconds = seconds.toString().padStart(2, 0);

        return hours > 0 ? (
            <span>
                {hours}:{minutes}:{seconds}
            </span>
        ) : (
            <span>
                {minutes}:{seconds}
            </span>
        );
    };

    render() {
        const { position, title, duration, artist } = this.props.trackDetails;

        return (
            <div className={`modal-background`}>
                <div className="modal-content">
                    <div className="track-details">
                        <div className="modal-track-rank">
                            <span>RANK</span>
                            {position}
                        </div>
                        <div className="modal-track-title">{title}</div>
                        by
                        <div className="modal-track-artist">{artist.name}</div>
                        <div className="modal-track-duration">
                            {this.durationFormatter(duration)}
                        </div>
                    </div>
                    <button onClick={this.props.onClose}>Close</button>
                </div>
            </div>
        );
    }
}
