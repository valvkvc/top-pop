import React from 'react';

export default class ListOrderSelector extends React.Component {
    state = {
        value: this.props.sortOrder,
    };

    handleSortOrderChange = (e) => {
        this.setState({ value: e.target.value });
        this.props.onSortOrderChange(e.target.value);
    };

    render() {
        return (
            <div className="select-sort-order">
                <label>
                    Sort by track duration:
                    <select
                        value={this.state.value}
                        onChange={this.handleSortOrderChange}
                    >
                        <option value="ascending">Shorter to longer</option>
                        <option value="descending">Longer to shorter</option>
                    </select>
                </label>
            </div>
        );
    }
}
