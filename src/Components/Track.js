import React from 'react';

export default function Track({ trackTitle, onTrackClick, id }) {
    const handleTrackClick = () => {
        onTrackClick(id);
    };

    return <li onClick={handleTrackClick}>{trackTitle}</li>;
}
